# FN America Invoice Bot

Greetings!

This bot was built to separate a master pdf containing multiple invoices into individual invoices, email copies of those individual invoices to the appropriate customer, and send a summary email to the process owner.

The bot is designed to be ran from an executable, so it's unnecessary to install any external libraries or software packages on the process owner's machine.

Run the command `pkg .` from inside the `FN-AMERICA_INVOICE` directory in order to build the executable.

Before running `pkg .`, `FN-AMERICA_INVOICE/subscriptionKey.txt` must be populated with the correct subscription key and `FN-AMERICA_INVOICE/utils/emailService.js` must be populated with the correct transporter data.

This bot's process is designed to be triggered by a cron on the process owner's machine.

The cron should be pointed at the executable that is appropriate for the process owner's machine (either `FN-AMERICA_INVOICE/fn-america-invoice-linux`, `FN-AMERICA_INVOICE/fn-america-invoice-macos`, or `FN-AMERICA_INVOICE/fn-america-invoice-win.exe`).

The bot looks for the presence of a master invoice pdf within `FN-AMERICA_INVOICE/userFiles/` in order to start doing it's job.

The bot relies on an excel file inside `FN-AMERICA_INVOICE/userFiles/` in order to retrieve the customer email data.

This excel file should be maintained and updated by the process owner as needed.

All errors and email confirmations can be found in `FN-AMERICA_INVOICE/log.txt`.



## Bot process:

	1. Checks for the presence of a file with 'INVOICE' in the file name within the /userFiles directory.

	2. If the file is present, it divides it into individual invoices and saves them as pdfs in the specified directory.

	3. It then reads an excel file named 'Customer Email List' within /userFiles.

	4. It uses the data from this file to email the invoices to their associated customer.

	5. Any customer who's email data isn't present in the excel file has their invoice(s) sent to process owner.

	6. A summary email is then sent to the process owner.

	7. The bot then cleans up after itself by deleting the contents of /separatedInvoicesTemp and the master invoice pdf from /userFiles.

	8. All errors and status are written in log.txt.
	

For questions contact: roman.emmons@plenadata.com