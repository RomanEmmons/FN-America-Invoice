const fs = require('fs');
const path = require('path');
const htmlString = require('../newInvoiceEmailHTML')
const createMap = require('./createMap');
const sleep = require('util').promisify(setTimeout)
const nodemailer = require('nodemailer');
const { env, dirPath } = require('../config');


const transporter = nodemailer.createTransport({
	service: '',
	auth: {
		user: '',
		pass: ''
	}
});

const stream = fs.createWriteStream(path.join(dirPath, '/log.txt'), { flags: 'a' });

// These variables are tracked in order to send totals to process owner.
let customersFoundInMap = [];
let customersWithEmailError = [];
let customersNOTFoundInMap = [];

const emailService = async () => {

	// Read excel file containing customer number/email map.
	const customerEmailStorageObj = createMap('./userFiles/Customer Email List.xlsx');

	const customersToEmailList = fs.readdirSync(path.join(dirPath, '/separatedInvoicesTemp'));

	for (const customerNumber of customersToEmailList) {

		// If customer email exists in Customer Email List.xlsx
		// email invoices to customer.
		if (customerEmailStorageObj[customerNumber]) {

			const customerName = customerEmailStorageObj[customerNumber][0];
			const customerEmail = customerEmailStorageObj[customerNumber][1];
			const attachments = await createAttachmentArray(customerNumber);

			const mailOptions = {
				from: 'DoNotReply@fnamerica.com',
				to: customerEmail,
				subject: 'New FN America, LLC Invoices',
				html: htmlString,
				attachments: attachments
			};

			await sendEmail(mailOptions, customerName, customerNumber);

			customersFoundInMap.push(customerName);

			// Delay to avoid email errors.
			await sleep(3000);

		}
		// If customer email doesn't exist in Customer Email List.xlsx
		// get customerName, and email invoices to process owner.
		else {

			const attachments = await createAttachmentArray(customerNumber);

			const mailOptions = {
				from: 'DoNotReply@fnamerica.com',
				to: 'roman.emmons@plenadata.com',
				cc: 'dev@plenadata.com',
				subject: null,
				html: htmlString,
				attachments: attachments
			};

			// Because customer isn't in mapping file, we must get customer name by parsing it from the file path.
			const splitArr = mailOptions.attachments[0].path.split('/');
			const customerName = splitArr[splitArr.length - 1].split(/IV\d{6,}/g)[0].trim();
			mailOptions.subject = `No Email Found For ${customerName}, Unable to Send Invoice(s)`;

			await sendEmail(mailOptions, customerName, customerNumber);

			customersNOTFoundInMap.push(customerName);

			// Delay to avoid email errors.
			await sleep(3000);

		}
	}

	// Adjust tracking variables once attempt has been made to send emails
	cleanErrorsFromFoundAndNOTFoundList();

	// Send confirmation email once bot has finished sending emails.
	sendCompletionEmailToProcessOwner();
};


// HELPER FUNCTIONS...

// This function returns an array containing all of the file paths
// to a customers invoices. The array is formatted
// so that the nodemailer can send all of the invoices
// along as attachments in mailOptions.
const createAttachmentArray = async (customerNumber) => {

	let attachments = []

	const invoicesToSendList = fs.readdirSync(path.join(dirPath, `/separatedInvoicesTemp/${customerNumber}`))

	for (const invoice of invoicesToSendList) {

		attachments.push({ path: path.join(dirPath, `/separatedInvoicesTemp/${customerNumber}/${invoice}`) })

	}

	return attachments;
};

// This function handles sending emails.
// It's also ugly af. I will refactor it if time permits.
const sendEmail = async (mailOptions, customerName, customerNumber) => {

	// Send separated invoice.
	transporter.sendMail(mailOptions, async (error, info) => {

		if (error) {
			console.log('Email NOT sent: ' + error);
			stream.write(`**ERROR SENDING EMAIL Customer: ${customerName} Time: ${new Date().toISOString()}` + "\n");

			// Send error email to process owner & roman if there's an error sending email.
			const errorMailOptions = {
				from: 'DoNotReply@fnamerica.com',
				cc: 'roman.emmons@plenadata.com',
				bc: 'dev@plenadata.com',
				to: 'Casey.Custer@fnamerica.com',
				subject: `Error Sending Invoice(s) to ${customerName}`,
				html: `
					<h2>Unable to send invoice(s) to ${customerName} due to:</h2>
					<h3>${error}</h3>
					<h3>Please email invoices associated with this customer on this date manually!</h3>
					<h3>For more information, check log.txt at ${new Date().toISOString()}</h3>
				`,
			};

			transporter.sendMail(errorMailOptions, function (error, info) {
				if (error) {
					console.log('Email NOT sent: ' + error);
					stream.write(`**ERROR SENDING EMAIL ERROR RESPONSE TO PROCESS OWNER Customer: ${customerName} Time: ${new Date().toISOString()}` + "\n");

				} else {
					console.log('Email sent: ' + info.response);
					stream.write(`**ERROR RESPONSE EMAIL SENT TO PROCESS OWNER Customer: ${customerName} Time: ${new Date().toISOString()}` + "\n");
				}
			});

			fs.rmdirSync(path.join(dirPath, `/separatedInvoicesTemp/${customerNumber}`), { recursive: true });

			customersWithEmailError.push(customerName);

		}

		else {
			console.log('Email sent: ' + info.response);
			stream.write(`**EMAIL SENT Customer: ${customerName} Time: ${new Date().toISOString()}` + "\n");

			fs.rmdirSync(path.join(dirPath, `/separatedInvoicesTemp/${customerNumber}`), { recursive: true });

		}
	});

};

// This function emails the tracking totals to the process owner.
const sendCompletionEmailToProcessOwner = () => {

	let foundList = '';
	let notFoundList = '';
	let errorList = '';
	var date = new Date().toLocaleDateString();
	const totalInvoicesProcessed = customersNOTFoundInMap.length + customersWithEmailError.length + customersFoundInMap.length;

	customersFoundInMap.forEach(customer => foundList += `<h5>${customer}</h5>`);
	customersNOTFoundInMap.forEach(customer => notFoundList += `<h5>${customer}</h5>`);
	customersWithEmailError.forEach(customer => errorList += `<h5>${customer}</h5>`);

	// Send confirmation email once bot has finished sending emails.
	const completeMailOptions = {
		from: 'DoNotReply@fnamerica.com',
		to: 'roman.emmons@plenadata.com',
		subject: `Invoice Processing Bot Results ${date}`,
		html: `
			<h2>Total number of invoices processed: ${totalInvoicesProcessed}</h2>

			<br></br>
			<h3>Customers that need to be emailed invoices manually due to missing map data (please update 'userFiles/Customer Email List.xlsx' with data for these customers):</h3>
			${notFoundList}
			<h5>Total: ${customersNOTFoundInMap.length}</h5>

			<br></br>
			<h3>Customers that need to be emailed manually due to an error encountered while sending email (please refer to error notification email and check mapping data for this customer in 'userFiles/Customer Email List.xlsx'):</h3>
			${errorList}
			<h5>Total: ${customersWithEmailError.length}</h5>

			<br></br>
			<h3>Customers that were emailed invoices successfully:</h3>
			${foundList}
			<h5>Total: ${customersFoundInMap.length}</h5>
		`,
	};

	transporter.sendMail(completeMailOptions, function (error, info) {
		if (error) {
			console.log('Email NOT sent: ' + error);
			stream.write(`**ERROR SENDING COMPLETION EMAIL TO PROCESS OWNER ${error} Time: ${new Date().toISOString()}` + "\n");

		} else {
			console.log('Email sent: ' + info.response);
			stream.write(`**COMPLETION EMAIL SUCCESSFULLY SENT TO PROCESS OWNER ${info.response} Time: ${new Date().toISOString()}` + "\n");
		}
	});

}

// This function cleans out all the customers that
// encountered and error while sending the invoice email
// from the the tracking variables
// customersFoundInMap and customersNOTFoundInMap.
const cleanErrorsFromFoundAndNOTFoundList = () => {

	let cleanedCustomersFoundInMap = [];
	let cleanedCustomersNOTFoundInMap = [];

	if (customersWithEmailError.length < 1) {
		return;
	}

	for (const erroredCustomer of customersWithEmailError) {

		// Handle cleaning for this erroredCustomer in customersFoundInMap.
		for (let i = 0; i < customersFoundInMap.length; i++) {

			const mappedCustomer = customersFoundInMap[i];

			if (erroredCustomer !== mappedCustomer) {

				// Populate cleanedCustomersFoundInMap with mappedCustomer
				// not found in customersWithEmailError.
				cleanedCustomersFoundInMap.push(mappedCustomer);
			}
		}

		// Handle cleaning for this erroredCustomer in customersNOTFoundInMap.
		for (let i = 0; i < customersNOTFoundInMap.length; i++) {

			const unmappedCustomer = customersNOTFoundInMap[i];

			if (erroredCustomer !== unmappedCustomer) {

				// Populate refactoredCustomersFoundInMap with unmappedCustomer
				// not found in customersWithEmailError.
				cleanedCustomersNOTFoundInMap.push(unmappedCustomer);
			}
		}
	}

	// Reset tracking array vars with freshly cleaned arrays.
	customersFoundInMap = cleanedCustomersFoundInMap;
	customersNOTFoundInMap = cleanedCustomersNOTFoundInMap;
}


module.exports = emailService;