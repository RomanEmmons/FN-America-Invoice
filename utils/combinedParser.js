
/**
 * @class BasePdfParser
 * @abstract
 * @desc Base class for all classes which parse and extract PDF data.
 */
class BasePdfParser {
	constructor(options = {}) {
		this.formatState = 'raw';
		this.rawData = undefined;
		this.thirdPartyData = undefined;
		this.yXData = {};
		this.sectionData = {};
		this.coordinatePrecision = options.coordinatePrecision !== undefined
			? options.coordinatePrecision : 3;
	}

	// eslint-disable-next-line class-methods-use-this
	async rawToThirdParty() {
		throw new Error('You must use a class defined in services/pdf/constructors/');
	}

	// eslint-disable-next-line class-methods-use-this
	async thirdPartyToYX() {
		throw new Error('You must use a class defined in services/pdf/constructors/');
	}

	async yXToSection() {
		if (this.formatState !== 'yX') {
			return;
		}

		// ZZZ TODO: this.sectionData = { ... };
		this.formatState = 'section';
	}

	round(number) {
		// https://medium.com/swlh/how-to-round-to-a-certain-number-of-decimal-places-in-javascript-ed74c471c1b8
		return Number(`${Math.round(`${number}e${this.coordinatePrecision}`)}e-${this.coordinatePrecision}`);
	}

	/**
	 * Checks to see if the document is corrupted
	 * Character codes less than 32 have so far shown to be a reliable indication
	 * of text that was not extracted properly.
	 * @returns {boolean}
	 */
	isCorrupted() {
		if (this.formatState !== 'yX') {
			throw new Error(`'isCorrupted()' encountered incompatible format state '${this.formatState}', expected 'yX'. Call 'thirdPartyToYX() before calling this method.`);
		}
		const text = this.grabAllText().join();
		// eslint-disable-next-line no-restricted-syntax
		for (const letter in text) {
			if (text.charCodeAt(letter) < 32) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Grab all pdf fields from the entire document
	 * @returns {Array} An array sorted in reading order, left to right, top to bottom
	 */
	grabAll() {
		if (this.formatState !== 'yX') {
			throw new Error(`'grabByAll()' encountered incompatible format state '${this.formatState}', expected 'yX'. Call 'thirdPartyToYX() before calling this method.`);
		}
		if (!this.allFields) {
			const allText = [];
			// return all objects in sorted ordered
			for (const yCoord of Object.keys(this.yXData)) {
				for (const xCoord of Object.keys(this.yXData[yCoord])) {
					allText.push(this.yXData[yCoord][xCoord]);
				}
			}
			this.allFields = allText
				.sort((a, b) => (a.y - b.y !== 0 ? a.y - b.y : a.x - b.x));
		}
		return this.allFields;
	}

	/**
	 * Grab all text from the pdf document
	 * @returns {Array} an Array of every text item on the page, in reading order
	 */
	grabAllText() {
		if (this.formatState !== 'yX') {
			throw new Error(`'grabByAll()' encountered incompatible format state '${this.formatState}', expected 'yX'. Call 'thirdPartyToYX() before calling this method.`);
		}
		const fields = this.grabAll();
		const strings = fields.map(f => f.text);
		return strings;
	}

	/**
	 * Grabs all Fields on a given page
	 * @param {Number} pageNumber The page number
	 * @returns {Array} An array of Fields from the yXData property of the Document
	 */
	grabByPage(pageNumber) {
		if (this.formatState !== 'yX') {
			throw new Error(`'grabByPage()' encountered incompatible format state '${this.formatState}', expected 'yX'. Call 'thirdPartyToYX() before calling this method.`);
		}
		if (pageNumber % 1 !== 0 || pageNumber < 1) {
			throw new Error(`Error in 'GrabByPage()': ${pageNumber} must be an integer greater than 0`);
		}
		const pageText = [];
		for (const yCoord of Object.keys(this.yXData)) {
			for (const xCoord of Object.keys(this.yXData[yCoord])) {
				// Determine if the text node belongs to the targeted page.
				if (this.yXData[yCoord][xCoord].page === pageNumber) {
					pageText.push(this.yXData[yCoord][xCoord]);
				}
				// Assume pages are ordered and return early if we iterate past the targeted page.
				else if (this.yXData[yCoord][xCoord].page > pageNumber) {
					return pageText;
				}
			}
		}
		return pageText;
	}

	/**
	 * Finds the first field containing text that matches the regular expression
	 * @param {RegExp|String} regex A regular expression or string
	 * @returns {Field} A pdf field from the yXData property of the document
	 */
	grabByRegex(regex) {
		if (this.formatState !== 'yX') {
			throw new Error(`'grabByPage()' encountered incompatible format state '${this.formatState}', expected 'yX'. Call 'thirdPartyToYX() before calling this method.`);
		}
		for (const yCoord of Object.keys(this.yXData)) {
			for (const xCoord of Object.keys(this.yXData[yCoord])) {
				// Determine if the text node contains a match to the regex
				if (this.yXData[yCoord][xCoord].text.match(regex)) {
					return this.yXData[yCoord][xCoord];
				}
			}
		}
		return null;
	}

	/**
	 * search for a field and return all the text on that line as a single string
	 * @param {RegExp|String} regex a regular expression or string
	 * @param {Object} options Object containing options parameters
	 * @param {String} options.Delimiter A character that separates each word in the line
	 * @returns {String} a line of concatenated text
	 */
	grabByRegexLine(regex, options = { Delimiter: ' ' }) {
		const field = this.grabByRegex(regex);
		return Object.values(this.yXData[field.y])
			.sort((a, b) => a.x - b.x)
			.map(f => f.text)
			.join(options.Delimiter);
	}

	/**
	 * @param {Point} topLeft includes .x and .y
	 * @param {Point} bottomRight includes .x and .y
	 * @returns {Array} containing all fields within the bounding box
	 */
	sectionalizeByBoundingBox(topLeft, bottomRight) {
		// find start and end indices of the table
		return this.grabAll().filter(field => field.x > topLeft.x
			&& field.y > topLeft.y
			&& field.x < bottomRight.x
			&& field.y < bottomRight.y);
	}

	// TODO: Add methods
	// sectionalizeByCoords()
	// sectionalizeByListOfCoords()

	// sectionalizeByColumn
	// - column name to start, grab everthing underneath until you reach a stopping regex
	// multipage. Default is take first


	/**
	 * Create a sction of fields from the document that starts and stops with regex
	 * @param {RegExp|String} startRegex
	 * @param {RegExp|String} stopRegex
	 * @param {Object} options Object containing options parameters
	 * @param {Boolean} options.AppendIndices add original index to fields as 'index' property
	 * @returns {Array} An array containing all pdf fields inside this section
	 */
	sectionalizeByRegex(startRegex, stopRegex, options = {}) {
		let startI;
		let stopI;
		// iterate over fields looking for the first start and stop match
		for (const [i, field] of this.grabAll().entries()) {
			if (!startI && field.text.match(startRegex)) {
				startI = i;
			}
			if (!stopI && startI && field.text.match(stopRegex)) {
				stopI = i;
			}
			if (startI && stopI && stopI > startI) {
				break;
			}
		}
		if (!(startI >= 0) || !(stopI >= 0)) {
			throw new Error('Could not find a range in this document that matched regex');
		}
		const section = this.allFields.slice(startI, stopI);

		return options.AppendIndices
			? section.map((f, i) => ({ ...f, index: startI + i })) : section;
	}

	/**
	 * Take a section and treat the first row as a column headers.
	 * Combines everything under each header into sections.
	 * @param {Array} section pdf fields
	 * @param {Object} options object containing parsing options
	 * @param {Array} options.Headers replace the found headers with custom names
	 * @param {Boolean} options.TextOnly only have text, not the fields
	 * @returns {Object} keys are the column names and values are a list of pdf fields
	 * directly underneath each header
	 */
	columnizeByFirstLineHeader(section, options = {}) {
		const headers = Object.values(this.yXData[section[0].y]);
		const headerText = options.Headers
			? options.Headers : headers.map(f => f.text);
		const columnCenters = this._findCenterOfFields(headers);
		const columns = {};
		for (const field of section.slice(headers.length)) {
			// find the section header closest to field and append it to the object list
			const index = this._findNearestNeighborX(field, columnCenters);
			if (columns[headerText[index]]) {
				columns[headerText[index]].push(options.TextOnly ? field.text : field);
			}
			else {
				columns[headerText[index]] = options.TextOnly ? [field.text] : [field];
			}
		}
		return columns;
	}

	// find the center of a pdf field
	_findCenterOfFields(field) {
		try {
			if (field instanceof Object && Array.isArray(field)) {
				return field.map(f => this._findCenterOfFields(f));
			}
			if (field instanceof Object && !(Array.isArray(field))) {
				return (field.x + (field.width / 2));
			}

			throw new Error('Data format not supported');

		}
		catch (exception) {
			console.log(exception.message);
			throw exception;
		}
	}

	// given a list of pdf fields, find the field that is closest
	// to the field of interest. Return the index of closest field
	_findNearestNeighborX(field, headerCenters) {
		const center = this._findCenterOfFields(field);
		const fieldCenterDifferences = headerCenters.map(f => Math.abs(f - center));
		return fieldCenterDifferences.indexOf(Math.min(...fieldCenterDifferences));
	}


	/**
	 * Take in a section that is assumed to be a table. Currently only supports horizontal
	 * tabular data. Returns an array of objects in ORM format
	 * @param {Array} section pdf fields output from a sectionalize call
	 * @param {Object} options an object for containing the tableize option profile
	 * @param {String} options.Veritcal_Fudge When finding lines, Tableize will look
	 * within a height range based on this value
	 * @param {String} options.Orientation 'Horizontal' is the only orientation currently supported,
	 * The first line of the section is treated as the header
	 * @param {Array} options.Header A list of header names to override the ones found on the table
	 * @returns {Array} Each index contains an ORM row from the table
	 */
	tableizeBySection(section, options = {}) {
		const opt = {
			// default values
			Veritcal_Fudge: 2,
			Orientation: 'Horizontal',
			...options
		};
		const startIndex = 0;
		const endIndex = section.length - 1;

		const table = [];
		if (opt.Orientation === 'Horizontal') {
			// find traditional line item rows found in tabular data
			const headerFields = Object.values(this.yXData[section[0].y]);
			let headerNames = [];
			// get the headers
			if (!opt.Header) {
				headerNames = section
					.slice(startIndex, endIndex)
					.filter(field => Math.abs(section[startIndex].y - field.y) < opt.Veritcal_Fudge)
					.map(field => field.text);
			}
			else {
				// TODO: assumes that the headers are the first thing in the section
				headerNames = opt.Header;
			}
			// Take the next element past the current row
			// find everything in that row
			// map elements in that row to an object based on column location
			// identify the last element of that line
			// repeat
			let currentIndex = startIndex + headerNames.length;
			while (currentIndex <= endIndex) {
				// only find lines within the indices bounded by the box passed in during options
				const line = section
					.slice(currentIndex, endIndex + 1)
					// eslint-disable-next-line no-loop-func
					.filter(f => Math.abs(section[currentIndex].y - f.y) < opt.Veritcal_Fudge)
					.sort((a, b) => a.x - b.x);

				const rowMap = [];
				// find nearest neighbors by horizontal center of headers
				const headerCenters = this._findCenterOfFields(headerFields);
				for (let i = 0; i < line.length; i++) {
					const columnValue = this._findNearestNeighborX(line[i], headerCenters);
					let tempColumnValue = columnValue;
					while (tempColumnValue > rowMap.length) {
						rowMap.push(null);
						tempColumnValue -= 1;
					}
					rowMap.push(line[i].text);
				}
				// fill end of table with null
				for (let i = line.length; i < headerNames.length; i++) {
					rowMap.push(null);
				}
				// combine the headers and the values under those headers
				// then turn them into an object that is added to the table

				const rowColumn = headerNames
					.map((header, i) => [header, rowMap[i]])
					.reduce((acc, curr) => ({ ...acc, [curr[0]]: curr[1] }), {});
				table.push(rowColumn);
				currentIndex += line.length;
			}
		}
		return table;
	}

	/**
	 * Takes a table that has a key column - one which is NEVER null and is ALWAYS a single line
	 * then it wraps all words and sentences that fall outside that pattern
	 * This essentially finds paragraphs in line item tabular data
	 * @param {Array} table An array that is in ORM table format, created by a tableize method
	 * @param {Object} options wrapping options
	 * @param {String} options.KeyColumn the column which is NEVER null and ALWAYS one line
	 * @returns {Array} Array of ORM mapped objects
	 */
	// eslint-disable-next-line class-methods-use-this
	wordWrap(table, options) {
		const rows = [];
		for (const row of table) {
			if (row[options.KeyColumn] !== null) {
				// row contains the required item: append it
				rows.push(row);
			}
			else {
				// row does not contain required item: look for word wrap
				for (const key of Object.keys(row)) {
					if (row[key] != null) {
						rows.slice(-1)[0][key] += ` ${row[key].trim()}`;
					}
				}
			}
		}
		return rows;
	}
}

// Dynamically mix in all of the mixins onto the base class.
let klass = BasePdfParser;

class CombinedPdfParser extends klass { }

module.exports.CombinedPdfParser = CombinedPdfParser;
