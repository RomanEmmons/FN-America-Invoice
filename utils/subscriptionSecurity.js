const fs = require('fs');
const path = require('path');
const { env, dirPath } = require('../config');


const subscriptionSecurity = async () => {

	const subscriptionPassContents = fs.readFileSync(dirPath + '/subscriptionKey.txt').toString();

	const attemptedKey = parseInt(subscriptionPassContents.match(/\d{8}/)[0]);

	const currentYear = new Date().getFullYear()

	const correctKey = currentYear * 32202;

	if (correctKey === attemptedKey) {

		return true;
	}
	else {

		return false;
	}

}


module.exports = subscriptionSecurity