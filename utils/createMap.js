const XLSX = require('xlsx');
const { env, dirPath } = require('../config');
const path = require('path');

// This function reads the customer email file
// and returns an object in which
// key values are tuples represented this way 
// customerNumber: [ customerName, customerEmail ]
const createMap = (pathToCustomerEmailList) => {

	const workbook = XLSX.readFile(path.join(dirPath, '/userFiles/Customer Email List.xlsx'));
	const sheet_name_list = workbook.SheetNames;
	const sheetJSON = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
	const map = {};

	for (const customer of sheetJSON) {

		map[customer['Customer #']] = [customer['Customer Name'], customer['Customer Email(s)']]

	}

	return map;

}

module.exports = createMap;