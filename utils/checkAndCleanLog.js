const fs = require('fs');
const path = require('path');

// This function checks if the log is over 10k lines long,
// if it is, it rewrites the log with only the last 1k lines.
const checkAndCleanLog = async () => {

	try {

		const logData = fs.readFileSync(path.join(__dirname, '../log.txt')).toString().split('**');

		let repopulateFileString = '';

		if (logData.length > 10000) {

			for (let i = 90000; i < logData.length; i++) {

				repopulateFileString += `**${logData[i]}`;

			}

			const repopulateFileStringBuffer = Buffer.from(repopulateFileString);

			fs.unlinkSync(path.join(__dirname, '../log.txt'));

			fs.writeFileSync(path.join(__dirname, '../log.txt'), repopulateFileStringBuffer);

			// Create stream to write to log.
			const stream = fs.createWriteStream(path.join(__dirname, '../log.txt'), { flags: 'a' });

			stream.write(`**LOG CLEARED OLDEST 9k LINES Time: ${new Date().toISOString()}` + "\n");
		}
	}

	catch (error) {

		throw new Error(`**Error Cleaning log: ${error}`)
	}
};

module.exports = checkAndCleanLog;