const { dirPath } = require('../config');
const pdfJs = require('pdfjs-dist/es5/build/pdf');
const { CombinedPdfParser } = require('./combinedParser');

/**
 * @class PdfJs
 * @desc Uses PDF.js to process the PDF and convert it to a standard format.
 */
module.exports.PdfJs = class PdfJs extends CombinedPdfParser {
	/**
	 * Creates a wrapper object around the PDF to be parsed.
	 * @example
	 * const pdf = new PdfJs(fileData);
	 * @param {String|Buffer} rawData A buffer or string containing the raw binary data of the PDF.
	 * @param {Object} options An object containing construction options such as coordinatePrecision
	 */
	constructor(rawData, options = {}) {
		super(options);
		this.rawData = rawData;
	}

	/**
	 * Converts the rawData provided to the constructor to the third party format.
	 * @example
	 * await pdf.rawToThirdParty();
	 * @param {Object?} options The non-default options to pass to getDocument.
	 */
	async rawToThirdParty(options = {}) {
		if (this.formatState !== 'raw') {
			return;
		}

		// Initialize document.
		this.thirdPartyData = await pdfJs.getDocument({
			...options,
			data: this.rawData
		}).promise;
		this.formatState = 'thirdParty';
	}

	/**
	 * Converts the data returned by the third party to [Y][X] format.
	 * @example
	 * await pdf.thirdPartyToYX();
	 */
	async thirdPartyToYX(options = {}) {
		if (this.formatState !== 'thirdParty') {
			return;
		}

		const allTextNodes = [];
		// PDF.js returns the coordinates of text nodes on the page which need to manipulated to
		// represent coordinates on the document.
		let pageOffset = 0;
		for (let i = 1; i <= this.thirdPartyData.numPages; i++) {
			// Get the page.
			const page = await this.thirdPartyData.getPage(i);

			// Get the max coordinates of the page.
			const [, , maxX, maxY] = page.view;

			// Get the list of text nodes for the page.
			const textNodes = (await page.getTextContent()).items;
			for (const textNode of textNodes) {

				// Extract the XY coordinates of the text node.
				let [, , , , x, y] = textNode.transform;

				// PDF.js uses the bottom left-hand corner as its coordinate axis but CombinedPdfParser uses
				// the top left-hand corner, so we invert the y-coordinate.
				y = maxY - y;

				// Offset Y if page 2 appears directly below page 1, offset X if page 2 appears to the right
				// of page 1. This method could be overridden to support more complex layouts.
				if (options.horizontalLayout) {
					x += pageOffset;
				}
				else {
					y += pageOffset;
				}

				// Round coordinates to `coordinatePrecision`.
				x = this.round(x);
				y = this.round(y);

				allTextNodes.push({
					y,
					x,
					text: textNode.str,
					dir: textNode.dir,
					page: page.pageNumber,
					transform: textNode.transform,
					fontName: textNode.fontName,
					// Round width and height to `coordinatePrecision`.
					width: this.round(textNode.width),
					height: this.round(textNode.height)
				});
			}

			// Offset Y if page 2 appears directly below page 1, offset X if page 2 appears to the right
			// of page 1. This method could be overridden to support more complex layouts.
			if (options.horizontalLayout) {
				pageOffset += maxX;
			}
			else {
				pageOffset += maxY;
			}
		}

		// Sort by vertical position, then by horizontal position.
		allTextNodes.sort((a, b) => (a.y - b.y !== 0 ? a.y - b.y : a.x - b.x));

		this.yXData = {};
		allTextNodes.forEach(textNode => {
			// Preserve order by making them all strings
			const xString = parseFloat(textNode.x).toFixed(this.coordinatePrecision);
			const yString = parseFloat(textNode.y).toFixed(this.coordinatePrecision);

			if (!this.yXData[yString]) {
				this.yXData[yString] = {};
			}
			this.yXData[yString][xString] = textNode;
		});
		this.formatState = 'yX';
	}
};
