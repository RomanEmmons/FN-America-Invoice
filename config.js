const path = require('path');

// Determine if we're running from a pkg binary.
// https://github.com/zeit/pkg/issues/358#issuecomment-415706989
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const isExecutable = process.pkg;

// Use path.dirname() instead of __dirname for referencing file outside of package.
// https://github.com/zeit/pkg/issues/195#issuecomment-322423769
const dirPath = isExecutable
	? path.dirname(process.execPath)
	: __dirname;

// Dynamically import environment variables.
// const envPath = path.join(dirPath, '/credentials.env')
// require('dotenv').config({ path: envPath });

// Load the environment from .env file.
const env = process.env;

const CONFIG = {
	env,
	dirPath,
	isExecutable
};

module.exports = CONFIG