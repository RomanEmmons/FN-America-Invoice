const { PdfJs } = require('./utils/pdfJs.constructor');
const fs = require('fs');
const path = require('path');
const { PDFDocument, componentsToColor } = require('pdf-lib');
const emailService = require('./utils/emailService');
const subscriptionSecurity = require('./utils/subscriptionSecurity')
const checkAndCleanLog = require('./utils/checkAndCleanLog');
const { env, dirPath } = require('./config');


// Functionality:

// look for master file in userFiles

// if there,

// read with pdfjs

// separate and save pdfs in separatedInvoicesTemp

// find email addresses in Customer Email List.xlsx

// email invoices to customers

// save invoices to this path...

// K:\Finance\_Finance Only\MCLN Invoicing

// once finished,  delete master file from userFiles and delete invoices from separatedInvoicesTemp

// if error, write to log.txt

// set cron for every 5 minutes 

// crontab -e

// */5 * * * * K:\Finance\_Finance Only\MCLN Invoicing/FN-America-Invoice/fn-america-invoice-win.exe  K:\Finance\_Finance Only\MCLN Invoicing/FN-America-Invoice/log.txt

// verify crontab -l

// set permission to run script (this may not be necessary)...

// chmod +x fn-america-invoice-win.exe

// command to build executable...

// pkg .


(async () => {

	/// Create stream to write to log.
	const stream = await fs.createWriteStream(dirPath + '/log.txt', { flags: 'a' });

	try {

		// Make sure subscription key is still valid.
		const subscriptionKeyValid = await subscriptionSecurity();

		if (!subscriptionKeyValid) {
			throw new Error('**Error: Subscription pass is incorrect, please make sure it hasn\'t expired!')
		}


		// With every attempted run, 
		// check length of log and clean accordingly.
		await checkAndCleanLog();

		let date;

		const userFileList = fs.readdirSync(path.join(dirPath, '/userFiles'))

		let masterInvoiceFileIndex;

		// Find invoice file in userFiles directory.
		for (let i = 0; i < userFileList.length; i++) {

			if (userFileList[i].includes('INVOICING')) {

				masterInvoiceFileIndex = i;
				stream.write(`**MASTER PDF DETECTED IN 'FN-America-Invoice/userFiles' Time: ${new Date().toISOString()}` + "\n");

				// File should be named in this format 'YYMMDD INVOICING.pdf'
				// so that date can be parsed from file name.
				// Date is then used to name the directory on K: drive
				// in which the separated invoices will be permanently saved.
				date = userFileList[i].match(/\d{6}/);

				// If the date isn't in file name,
				// create one based on current date.
				if (!date) {

					const dateObj = new Date();
					let month = dateObj.getUTCMonth() + 1;
					month = month.toString(); //months from 1-12
					const day = dateObj.getUTCDate().toString();
					const year = dateObj.getUTCFullYear().toString().slice(-2);

					// Pad with zero if 1 digit date.
					if (month.length < 2) {

						month = `${'0'}${month}`;
					}

					// Pad with zero if 1 digit date.
					if (day.length < 2) {

						day = `${'0'}${day}`;
					}

					date = `${year}${month}${day}`;
				}

				// Test to see if dir exists before we try to mkdir.
				try {

					// Try block is for production.
					try {

						// Save to K: drive.
						fs.readdirSync(`K:\\Finance\\_Finance Only\\MCLN Invoicing/${date}`);
					}

					// Catch block is for development.
					catch {

						// For development, we checking within directory.
						fs.readdirSync(path.join(dirPath, '..', `/FN-America-Invoice/${date}`));
					}

				}

				// If dir test fails, we can safely mkdir in K: drive.
				catch {

					// Try block is for production.
					try {

						fs.mkdirSync(`K:\\Finance\\_Finance Only\\MCLN Invoicing/${date}`);
					}

					// Catch block is for development.
					catch {

						// For development, we are saving within FN-America-Invoice dir.
						fs.mkdirSync(path.join(dirPath, '..', `/FN-America-Invoice/${date}`));
					}

				}
			}
		}

		// Stop function if no master file found. There is no need to continue.
		if (masterInvoiceFileIndex === undefined) {

			throw new Error(`NO MASTER PDF PRESENT IN 'FN-America-Invoice/userFiles' DIRECTORY Time: ${new Date().toISOString()}`)
		};

		// Read the master pdf.
		const pdfBufferMaster = fs.readFileSync(path.join(dirPath, '/userFiles', userFileList[masterInvoiceFileIndex]));

		// Parse data and get all fields
		const pdfDoc = new PdfJs(pdfBufferMaster);
		await pdfDoc.rawToThirdParty();
		await pdfDoc.thirdPartyToYX();
		const allFields = pdfDoc.grabAll();

		// Declare variables then loop through allFields to extract data
		// necessary to segment master and save files.
		let invoiceDataStorage = [];
		let currentStartPage = 1;
		let currentInvoiceData = {};

		for (let i = 1; i < allFields.length; i++) {

			const field = allFields[i]

			// If field.text contains 'Bill To:',
			// it also contains the customer number,
			// and the next field contains the customerName.
			if (field.text.includes('Bill To:')) {

				// Extract and save customer number.
				currentInvoiceData['customerNumber'] = field.text.match(/\d{8}/g)[0];

				// customerNameField will almost always be the field after 'Bill To:'
				// and has a variable amount of whitespace in front and after the name.
				let customerNameFieldText = allFields[i + 1].text.trim().split('   ')[0];

				// sometimes it isn't though because the pdf is built different.
				// the variation we've seen has customerNameField two indexes after 'Bill To:'
				// we can test for this by checking if customerNameFieldText var includes 'Ship To:' 
				if (customerNameFieldText.includes('Ship To:')) {
					customerNameFieldText = allFields[i + 2].text.trim().split('   ')[0];
				}

				currentInvoiceData['customerName'] = customerNameFieldText.replace('&', '+');

			};

			// Look for 'Invoice:' to find invoice number.
			if (field.text.includes('Invoice: ')) {

				// Extract and save invoice number.
				currentInvoiceData['invoiceNumber'] = field.text.match(/IV\d{6,}/g)[0];

			}

			// Look for '                             Total:' to indicate the end of the current invoice.
			if (field.text.includes('                                                    Total:')) {

				currentInvoiceData['startPage'] = currentStartPage;
				currentInvoiceData['endPage'] = field.page;

				invoiceDataStorage.push(currentInvoiceData);

				currentStartPage = field.page += 1;

				currentInvoiceData = {};

			}
		}

		// Load a PDFDocument from pdfBufferMaster
		const donorPdfDoc = await PDFDocument.load(pdfBufferMaster);

		for (const invoiceData of invoiceDataStorage) {

			// Create a new PDFDocument
			const separatedInvoice = await PDFDocument.create();

			// Loop through each page number that is to be populated in separated invoice.
			for (let i = invoiceData.startPage; i <= invoiceData.endPage; i++) {

				// Copy pages from master donorPdfDoc.
				const [firstDonorPage] = await separatedInvoice.copyPages(donorPdfDoc, [i - 1])

				// Add the copied page.
				separatedInvoice.addPage(firstDonorPage);

			}

			// Saves newly created invoice as a Uint8Array so it can be written to disk with fs.
			const separatedInvoiceBytes = await separatedInvoice.save();


			// THIS CODE BLOCK SAVES INVOICES IN TEMP FILE.
			// THESE INVOICES WILL BE EMAILED AND DELETED.
			// Check if dir already exists
			const separatedInvoicesDirList = fs.readdirSync(path.join(dirPath, '/separatedInvoicesTemp')).join(',');

			// If dir exists already, write invoice to dir.
			if (separatedInvoicesDirList.includes(invoiceData.customerNumber)) {

				// Create separated pdf in temp file.
				fs.writeFileSync(path.join(dirPath, '..', `/FN-America-Invoice/separatedInvoicesTemp/${invoiceData.customerNumber}/${invoiceData.customerName} ${invoiceData.invoiceNumber}.pdf`), separatedInvoiceBytes);


			} else {

				// Else, make dir then write file.
				fs.mkdirSync(path.join(dirPath, '..', `/FN-America-Invoice/separatedInvoicesTemp/${invoiceData.customerNumber}`));

				// Create separated pdf in temp file.
				fs.writeFileSync(path.join(dirPath, '..', `/FN-America-Invoice/separatedInvoicesTemp/${invoiceData.customerNumber}/${invoiceData.customerName} ${invoiceData.invoiceNumber}.pdf`), separatedInvoiceBytes);

			}

			// Try block is for production.
			try {

				// Save to K: drive.
				fs.writeFileSync(`K:\\Finance\\_Finance Only\\MCLN Invoicing/${date}/${invoiceData.customerName} ${invoiceData.invoiceNumber}.pdf`, separatedInvoiceBytes);
			}

			// Catch block is for development.
			catch {

				// For development, we are saving within FN-America-Invoice dir.
				fs.writeFileSync(path.join(dirPath, '..', `/FN-America-Invoice/${date}/${invoiceData.customerName} ${invoiceData.invoiceNumber}.pdf`), separatedInvoiceBytes);
			}

		};

		// Email service loops through separatedInvoices
		// creates an attachment object and emails it to the appropriate customer.
		// It also deletes the directories after emailing the invoices.
		await emailService();

		// Delete master pdf.
		fs.unlinkSync(path.join(path.join(dirPath, '/userFiles'), userFileList[masterInvoiceFileIndex]));

		// Log success to txt file.
		stream.write(`**FINISHED SUCCESSFULLY Time: ${new Date().toISOString()}` + "\n");
	}

	catch (error) {

		// Log error.
		console.error(error);
		stream.write(`**${error} Time: ${new Date().toISOString()}` + "\n");
	}
})();
